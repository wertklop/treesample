import com.intellij.icons.AllIcons;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.SimpleToolWindowPanel;
import com.intellij.openapi.wm.ToolWindowAnchor;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.openapi.wm.ex.ToolWindowEx;
import com.intellij.ui.SimpleTextAttributes;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import com.intellij.ui.content.ContentManager;
import com.intellij.ui.treeStructure.*;
import com.intellij.util.DisposeAwareRunnable;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import static com.intellij.ui.ScrollPaneFactory.createScrollPane;

/**
 * @author Gennadii Kurbatov
 * @since 06.10.18
 */
public class TreeSampleComponent implements ProjectComponent {

    private Tree tree;
    private Node rootNode;
    private Project project;
    private ToolWindowEx toolWindow;
    private SimpleTreeBuilder treeBuilder;

    public TreeSampleComponent(Project project) {
        this.project = project;
    }

    @Override
    public void initComponent() {
        DumbService.getInstance(project).runWhenSmart(DisposeAwareRunnable.create(() -> {
            initTree();
            treeBuilder = new SimpleTreeBuilder(tree, (DefaultTreeModel) tree.getModel(), new TreeStructure(), null);
            rootNode = new Node(project, null, "Root node");
            treeBuilder.initRootNode();
            treeBuilder.expand(rootNode, null);


            SimpleToolWindowPanel panel = new SimpleToolWindowPanel(true, true);

            ToolWindowManager manager = ToolWindowManager.getInstance(project);
            toolWindow = (ToolWindowEx) manager.registerToolWindow("TreeSampleComponent", false, ToolWindowAnchor.RIGHT, project, true);
            toolWindow.setIcon(AllIcons.General.Add);

            Content content = ContentFactory.SERVICE.getInstance().createContent(panel, null, false);
            ContentManager contentManager = toolWindow.getContentManager();
            contentManager.addContent(content);
            contentManager.setSelectedContent(content, false);

            panel.setToolbar(createToolbarPanel());
            panel.setContent(createScrollPane(tree));
        }, project));
    }

    private JComponent createToolbarPanel() {
        DefaultActionGroup toolbarActionGroup = new DefaultActionGroup();
        toolbarActionGroup.add(new AddAction());
        toolbarActionGroup.add(new RemoveAction());

        ActionManager actionManager = ActionManager.getInstance();
        ActionToolbar actionToolbar = actionManager.createActionToolbar("Toolbar", toolbarActionGroup, true);
        actionToolbar.setTargetComponent(tree);

        return actionToolbar.getComponent();
    }

    private void initTree() {
        JTextPane textPane = new JTextPane();
        textPane.setOpaque(false);

        tree = new Tree();

        textPane.setFont(tree.getFont());
        textPane.setBackground(tree.getBackground());
        textPane.setForeground(tree.getForeground());

        tree.getEmptyText().clear();
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
    }

    private class TreeStructure extends SimpleTreeStructure {
        @Override
        public Object getRootElement() {
            return rootNode;
        }
    }

    private class Node extends CachingSimpleNode {

        private String name;

        Node(Project project, Node parent, String name) {
            super(project, parent);
            this.name = name;
        }

        @Override
        protected SimpleNode[] buildChildren() {
            return NO_CHILDREN;
        }

        @Override
        protected void doUpdate() {
            setNameAndTooltip(name);
        }

        private void setNameAndTooltip(String name) {
            getTemplatePresentation().clearText();
            getTemplatePresentation().addText(new ColoredFragment(name, null, prepareAttributes(getPlainAttributes())));
        }

        private SimpleTextAttributes prepareAttributes(SimpleTextAttributes from) {
            return new SimpleTextAttributes(from.getBgColor(), from.getFgColor(), null, from.getStyle());
        }
    }

    private class AddAction extends AnAction {

        AddAction() {
            super("Add node", "", AllIcons.General.Add);
        }

        @Override
        public void actionPerformed(@NotNull AnActionEvent e) {
            treeBuilder.queueUpdateFrom(new Node(project, rootNode, "node"), true);
        }
    }

    private class RemoveAction extends AnAction {

        RemoveAction() {
            super("Remove node", "", AllIcons.General.Remove);
        }

        @Override
        public void actionPerformed(@NotNull AnActionEvent e) {
            // ????
        }
    }
}
